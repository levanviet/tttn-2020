use spring;

insert into role(code,name) values('ADMIN','Quản trị');
insert into role(code,name) values('USER','Người dùng');

insert into user(username,password,fullname,status,roleid)
values('admin','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG','admin',1,1);

insert into user(username,password,fullname,status,roleid)
values('nguyenvana','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG','nguyễn văn A',1,2);
insert into user(username,password,fullname,status,roleid)
values('nguyenvanb','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG','nguyễn văn B',1,2);
insert into user(username,password,fullname,status,roleid) values('vietanh','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG',N'Le Van Viet',1,1);

select *from role;
INSERT INTO user_role(userid,roleid) VALUES (1,1);
INSERT INTO user_role(userid,roleid) VALUES (2,2);
INSERT INTO user_role(userid,roleid) VALUES (3,2);
select *from user;
#Thể loại;

insert into category(name,code) values(N'Áo','ao');
insert into category(name,code) values(N'Đầm','dam');
insert into category(name,code) values(N'Váy','vay');
insert into category(name,code) values(N'Quần','quan');
insert into category(name,code) values(N'Áo khoác','giay');
insert into category(name,code) values(N'giầy','giay');
insert into category(name,code) values(N'Túi xách','tui-xach');
insert into category(name,code) values(N'Thắt lưng','that-lung');
insert into category(name,code) values(N'Khăn','that-lung');
insert into category(name,code) values(N'Bông tai lưng','that-lung');
insert into category(name,code) values(N'Vòng cổ','vong-co');

select *from category;
select *from spring.product;
#Áo 
insert into product(name,code,price,categoryid) values (N'Áo kiểu, Dáng mới 20SSME017D','ao-kieu-dang-moi-20ssme017d',699000,1);
insert into product(name,code,price,categoryid) values (N'Sơ mi tay lỡ, Dáng croptop 20SSME058F','so-mi-tay-lo-dang-croptop-20SSME058F',599000,1);
insert into product(name,code,price,categoryid) values (N'Sơ mi tay dài, Dáng sơ vin 20SSME015D','so-mi-tay-dai-dang-so-vin-20SSME015D',799000,1);
insert into product(name,code,price,categoryid) values (N'Áo sát nách,  20SANE002O','ao-sat-nach-20SANE002O',399000,1);
insert into product(name,code,price,categoryid) values (N'Áo kiểu, Dáng mới 20SSME017H','ao-kieu-dang-moi-20SSME017H',699000,1);
insert into product(name,code,price,categoryid) values (N'Sơ mi tay ngắn, Dáng sơ vin 20SSME031X','so-mi-tay-ngan-dang-so-vin-20SSME031X',599000,1);
insert into product(name,code,price,categoryid) values (N'Áo sát nách,  20SSME034D','ao-sat-nach-20SSME034D',599000,1);
insert into product(name,code,price,categoryid) values (N'Sơ mi tay ngắn, Dáng bo gấu 20SSME037F','so-mi-tay-ngan-dang-bo-gau-20SSME037F',699000,1);
insert into product(name,code,price,categoryid) values (N'Áo croptop,  20SSME024D2','ao-croptop-20SSME024D2',599000,1);

#Dam
insert into product(name,code,price,categoryid) values (N'Đầm Maxi, Dáng thắt eo 19ADKE296D','dam-maxi-dang-that-eo-19ADKE296D',1499000,2);
insert into product(name,code,price,categoryid) values (N'Đầm tay lỡ, Dáng suông 19ADKE280G','dam-tay-lo-dang-suong-19ADKE280G',999000,2);
insert into product(name,code,price,categoryid) values (N'Đầm tay ngắn, Dáng thắt eo 20SDKE018H','dam-tay-ngan-dang-that-eo-20SDKE018H',1499000,2);
insert into product(name,code,price,categoryid) values (N'Đầm tay lỡ, Dáng suông 19ADKE280B','dam-tay-lo-dang-suong-19ADKE280B',999000,2);
insert into product(name,code,price,categoryid) values (N'Đầm tay ngắn, Dáng thắt eo 19ADKE276C','dam-tay-ngan-dang-that-eo-19ADKE276C',1499000,2);
insert into product(name,code,price,categoryid) values (N'Đầm tay ngắn, Dáng thắt eo 20SDKE022T','dam-tay-ngan-dang-that-eo-20SDKE022T',1299000,2);
insert into product(name,code,price,categoryid) values (N'Đầm sát nách, Dáng thắt eo 19ADKE286X','dam-sat-nach-dang-that-eo-19ADKE286X',999000,2);
insert into product(name,code,price,categoryid) values (N'Đầm tay ngắn, Dáng thắt eo 19ADKE347R1','dam-tay-ngan-dang-that-eo-19ADKE347R1',999000,2);
insert into product(name,code,price,categoryid) values (N'Đầm tay ngắn, Dáng thắt eo 19ADKE347R2','dam-tay-ngan-dang-that-eo-19ADKE347R2',1499000,2);




Select *from product where id =3;




















































































































