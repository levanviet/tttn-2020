create database springmvcbasicfree

insert into role(name,code) values('Người dùng','USER');
insert into role(name,code) values('Quản trị','ADMIN');

insert into user(username,password,fullname,status) values('vietanh','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG',N'Lê Văn Việt',1);
insert into user(username,password,fullname,status) values('manhdo','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG',N'Đặng Văn Mạnh',1);
insert into user(username,password,fullname,status) values('sonden','$2a$10$/RUbuT9KIqk6f8enaTQiLOXzhnUkiwEJRdtzdrMXXwU7dgnLKTCYG',N'Nguyễn Hồng Sơn',1);

insert into user_role(userid,roleid) values(1,2);
insert into user_role(userid,roleid) values(2,1);
insert into user_role(userid,roleid) values(3,1);

