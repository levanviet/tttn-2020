package com.laptrinhjavaweb.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.laptrinhjavaweb.model.RoleModel;
import com.laptrinhjavaweb.model.UserModel;

public class UserMapper implements RowMapper<UserModel> {

	@Override
	public UserModel mapRow(ResultSet resultSet) {
		UserModel user = new UserModel();
		try {
			user.setId(resultSet.getLong("id"));
			user.setName(resultSet.getString("name"));
			user.setPassWord(resultSet.getString("password"));
			user.setFullName(resultSet.getString("fullname"));
			
			RoleModel role = new RoleModel();
			role.setCode(resultSet.getString("code"));
			user.setRole(role);
			if(resultSet.getString("createdby") != null) {
				user.setCreatedBy(resultSet.getString("createdby"));
			}
			if(resultSet.getString("modifiedby") != null) {
				user.setModifiedBy(resultSet.getString("modifiedby"));
			}
			if(resultSet.getTimestamp("createddate") != null) {
				user.setCreatedDate(resultSet.getTimestamp("createddate"));
			}
			if(resultSet.getTimestamp("createddate") != null) {
				user.setModifiedDate(resultSet.getTimestamp("modifieddate"));
			}
			return user;
		} catch (SQLException e) {
			return null;
		}

	}

}
