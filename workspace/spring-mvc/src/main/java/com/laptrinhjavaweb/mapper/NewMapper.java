package com.laptrinhjavaweb.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.laptrinhjavaweb.model.NewsModel;

public class NewMapper implements RowMapper<NewsModel> {

	@Override
	public NewsModel mapRow(ResultSet resultSet) {
		try {
			NewsModel model = new NewsModel();
			model.setId(resultSet.getLong("id"));
			model.setTitle(resultSet.getString("title"));
			model.setContent(resultSet.getString("content"));
			model.setCategory_id(resultSet.getLong("category_id"));
			
			if(resultSet.getString("modifiedby") != null) {
				model.setModifiedBy(resultSet.getString("modifiedby"));
			}
			if(resultSet.getTimestamp("modifieddate") != null) {
				model.setModifiedDate(resultSet.getTimestamp("modifieddate"));
			}
			if(resultSet.getString("createdby") != null) {
				model.setCreatedBy(resultSet.getString("createdby"));
			}
			if(resultSet.getTimestamp("createddate") != null) {
				model.setCreatedDate(resultSet.getTimestamp("createddate"));
			}
			if(resultSet.getString("thumbnail") != null) {
				model.setThumbnail(resultSet.getString("thumbnail"));
			}
			return model;
		} catch (SQLException e) {
			return null;
		}
		

	}
}
