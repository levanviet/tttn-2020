package com.laptrinhjavaweb.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.laptrinhjavaweb.model.ProductModel;

public class ProductMapper implements RowMapper<ProductModel>{

	@Override
	public ProductModel mapRow(ResultSet resultSet) {
		
		try {
			ProductModel model = new ProductModel();
			model.setId(resultSet.getLong("id"));
			model.setName(resultSet.getString("name"));
			model.setCode(resultSet.getString("code"));
			model.setCategoryid(resultSet.getLong("categoryid"));
			model.setPrice(resultSet.getFloat("price"));
			model.setThumbnail(resultSet.getString("thumbnail"));
			model.setSize(resultSet.getString("size"));
			
//			if(resultSet.getString("modifiedby") != null) {
//				model.setModifiedBy(resultSet.getString("modifiedby"));
//			}
//			if(resultSet.getTimestamp("modifieddate") != null) {
//				model.setModifiedDate(resultSet.getTimestamp("modifieddate"));
//			}
//			if(resultSet.getString("createdby") != null) {
//				model.setCreatedBy(resultSet.getString("createdby"));
//			}
//			if(resultSet.getTimestamp("createddate") != null) {
//				model.setCreatedDate(resultSet.getTimestamp("createddate"));
//			}
//			if(resultSet.getString("thumbnail") != null) {
//				model.setThumbnail(resultSet.getString("thumbnail"));
//			}
			return model;
		} catch (SQLException e) {
			return null;
		}
		
		
	}

}
