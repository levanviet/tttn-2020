package com.laptrinhjavaweb.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class ProductEntity extends BaseEntity{

	@Column(name="code")
	private String code;
	
	@Column(name="name")
	private String name;
	
	@Column(name="thumbnail")
	private String thumbnail;
	
	@Column(name="price")
	private float price;
	
	@Column(name="quantity")
	private int quantity;
	
	@Column(name="size")
	private String size;
	
	
	@ManyToOne
	@JoinTable(name = "product_category", joinColumns = @JoinColumn(name = "product_id"), 
	  inverseJoinColumns = @JoinColumn(name = "category_id"))
	private CategoryEntity category;
	
}
