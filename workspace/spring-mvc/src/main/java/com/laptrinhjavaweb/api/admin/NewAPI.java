package com.laptrinhjavaweb.api.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.laptrinhjavaweb.model.ProductModel;
import com.laptrinhjavaweb.service.IProductService;

@RestController(value = "NewAPIOfAdmin")
public class NewAPI {

	@Autowired
	private IProductService productService;
	
	@GetMapping("api/new/all")
	public List<ProductModel> getAll(@RequestBody ProductModel model) {
		return productService.findAll();
	}
	@PostMapping("api/new/add")
	public ProductModel createNew(@RequestBody ProductModel model) {
		return productService.save(model);
	}
	
	@PutMapping("api/new/update")
	public ProductModel updateNew(@RequestBody ProductModel model) {
		return productService.update(model);
	}
	
	@DeleteMapping("api/new/delete")
	public void deleteNew(@RequestBody ProductModel model) {
		productService.delete(model);
	}
}
