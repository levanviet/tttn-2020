package com.laptrinhjavaweb.dto;

public class UserDTO extends AbstractDTO<UserDTO>{
	
	private String name;
	private String passWord;
	private String fullName;
	private Long role_ID;
	private int status;
	private RoleDTO role = new RoleDTO();
	public RoleDTO getRole() {
		return role;
	}
	public void setRole(RoleDTO role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Long getRole_ID() {
		return role_ID;
	}
	public void setRole_ID(Long role_ID) {
		this.role_ID = role_ID;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
