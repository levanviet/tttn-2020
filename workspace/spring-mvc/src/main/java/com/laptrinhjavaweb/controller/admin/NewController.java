package com.laptrinhjavaweb.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.laptrinhjavaweb.model.CategoryModel;
import com.laptrinhjavaweb.model.ProductModel;
import com.laptrinhjavaweb.paging.PageRequest;
import com.laptrinhjavaweb.paging.Pageble;
import com.laptrinhjavaweb.service.ICategoryService;
import com.laptrinhjavaweb.service.IProductService;
import com.laptrinhjavaweb.sort.Sortter;

@Controller(value = "newControllerOfAdmin")
public class NewController {
	
	@Autowired
	private IProductService productService;
	
	@Autowired
	private ICategoryService categoryService;
	
	
	@RequestMapping(value = "/admin/new/list", method = RequestMethod.GET)
	public ModelAndView listNew(@ModelAttribute("models") ProductModel model) {
		ModelAndView mav = new ModelAndView("admin/news/new");
		Pageble pageble = new PageRequest(model.getPage(), model.getMaxPageItem(),
				new Sortter(model.getSortName(), model.getSortBy()));
		model.setListResult(productService.findProductsByCategoryId(pageble, model.getCategoryid()));
		model.setTotalItem(productService.getTotalItem());
		model.setTotalPage((int) Math.ceil((double) model.getTotalItem() / model.getMaxPageItem()));

		mav.addObject("models", model);
		return mav;
	}
	@RequestMapping(value = "/admin/new/edit", method = RequestMethod.GET)
	public ModelAndView editNew(@ModelAttribute("models,categotys") ProductModel model,CategoryModel categorys) {
		ModelAndView mav = new ModelAndView("admin/news/edit");
		model = productService.findOne(model.getId());
		categorys.setListResult(categoryService.findAll());
		mav.addObject("models", model);	
		mav.addObject("categorys", categorys);
		return mav;
	}
}