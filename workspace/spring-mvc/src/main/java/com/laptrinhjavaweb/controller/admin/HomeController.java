package com.laptrinhjavaweb.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.laptrinhjavaweb.model.CategoryModel;
import com.laptrinhjavaweb.model.ProductModel;
import com.laptrinhjavaweb.service.ICategoryService;
import com.laptrinhjavaweb.service.IProductService;

@Controller(value = "homeControllerOfAdmin")
public class HomeController {

	@Autowired
	private IProductService productService;
	
	@Autowired
	private ICategoryService categoryService;
	@RequestMapping(value = "/admin-home", method = RequestMethod.GET)
	public ModelAndView homePage(@ModelAttribute("categorys,models") CategoryModel categprys,ProductModel model) {
		ModelAndView mav = new ModelAndView("admin/home");
		model.setListResult(productService.findAll());
		categprys.setListResult(categoryService.findAll());
		mav.addObject("models", model);
		return mav;
	}
}