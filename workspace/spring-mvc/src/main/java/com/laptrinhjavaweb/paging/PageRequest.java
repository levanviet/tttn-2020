package com.laptrinhjavaweb.paging;
import com.laptrinhjavaweb.sort.Sortter;
public class PageRequest implements Pageble{

	private Integer page;
	private Integer maxPageItem;
	private Sortter sortter;
	
	public PageRequest() {
		super();
	}
	public PageRequest(Integer page,Integer maxPageItem,Sortter sortter) {
		this.page = page;
		this.maxPageItem = maxPageItem;
		this.sortter = sortter;
	}
	@Override
	public Integer getOffset() {
		if(page != null && maxPageItem != null) {
			return (this.page - 1 ) * this.maxPageItem;
		}
		return null;
	}

	@Override
	public Integer getLimit() {
		return this.maxPageItem;
	}

	@Override
	public Integer getPage() {
		return this.page;
	}
	
	public Sortter getSortter() {
		if(sortter != null) {
			return this.sortter;
		}
		return null;
	}
	public void setSortter(Sortter sortter) {
		this.sortter = sortter;
	}

}
