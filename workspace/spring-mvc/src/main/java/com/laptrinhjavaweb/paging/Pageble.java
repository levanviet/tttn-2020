package com.laptrinhjavaweb.paging;

import com.laptrinhjavaweb.sort.Sortter;

public interface Pageble {
	Integer getOffset();
	Integer getLimit();
	Integer getPage();
	Sortter getSortter();
}
