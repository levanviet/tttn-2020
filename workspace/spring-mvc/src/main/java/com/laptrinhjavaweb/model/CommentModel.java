package com.laptrinhjavaweb.model;


public class CommentModel extends AbstractModel<CommentModel>{

	private Long user_ID;
	private Long news_ID;
	private String content;


	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Long getUser_ID() {
		return user_ID;
	}
	public void setUser_ID(Long user_ID) {
		this.user_ID = user_ID;
	}
	public Long getNews_ID() {
		return news_ID;
	}
	public void setNews_ID(Long news_ID) {
		this.news_ID = news_ID;
	}
	
}
