package com.laptrinhjavaweb.dao.impl;

import java.util.List;

import com.laptrinhjavaweb.dao.ICommentDAO;
import com.laptrinhjavaweb.mapper.CommentMapper;
import com.laptrinhjavaweb.model.CommentModel;

public class CommentDAO extends AbstractDAO<CommentModel> implements ICommentDAO{

	@Override
	public void delete(Long id) {
		String sql = "delete from comment where id = ?";
		update(sql, id);
	}

	@Override
	public List<CommentModel> findAllNewIDByComment(Long id) {
		String sql = "select *from comment where id = ?";
		return query(sql, new CommentMapper(), id);
	}

	@Override
	public void Edit(CommentModel comment) {
		StringBuffer sql = new StringBuffer("update comment set content = ? , ");
		sql.append("createddate = ?,");
		sql.append("modifieddate = ?,");
		sql.append("createdby? = ?,");
		sql.append("modifiedby = ?");
		sql.append(" where id = ?");
		update(sql.toString(), comment);	
	}

	@Override
	public Long save(CommentModel comment) {
		StringBuffer sql = new StringBuffer("insert into comment ");
		sql.append("(content, ");
		sql.append("user_id, ");
		sql.append("news_id, ");
		sql.append("createddate, ");
		sql.append("createdby) ");
		sql.append("values (?, ?, ?, ?, ?)");
		return insert(sql.toString(),
				comment.getContent(),
				comment.getUser_ID(),
				comment.getNews_ID(),
				comment.getCreatedDate(),
				comment.getCreatedBy());
	}

	@Override
	public CommentModel findOne(Long id) {
		StringBuffer sql = new StringBuffer("select *from id where id = ?");
		List<CommentModel> comment = query(sql.toString(),new CommentMapper() ,id);
		return comment.isEmpty() ? null : comment.get(0);
	}

	@Override
	public void deleteByNewID(Long id) {
		String sql = "delete from comment where news_id = ?";
		update(sql, id);	
	}

}
