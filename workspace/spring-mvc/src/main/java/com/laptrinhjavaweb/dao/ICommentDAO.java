package com.laptrinhjavaweb.dao;

import java.util.List;
import com.laptrinhjavaweb.model.CommentModel;
public interface ICommentDAO{
	void delete(Long id);
	List<CommentModel> findAllNewIDByComment(Long id);
	void Edit(CommentModel comment); 
	Long save(CommentModel comment);
	CommentModel findOne(Long id);
	void deleteByNewID(Long id);
}
