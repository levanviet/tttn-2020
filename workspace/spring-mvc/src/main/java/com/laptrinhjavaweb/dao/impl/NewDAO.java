package com.laptrinhjavaweb.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.laptrinhjavaweb.dao.INewDAO;


import com.laptrinhjavaweb.mapper.NewMapper;
import com.laptrinhjavaweb.model.NewsModel;
import com.laptrinhjavaweb.paging.Pageble;


@Repository
public class NewDAO extends AbstractDAO<NewsModel> implements INewDAO {

	@Override
	public List<NewsModel> findCategoryIDbyNews(Long categoryID) {
		String sql = "select *from new where category_id = ?";
		return query(sql, new NewMapper(), categoryID);

	}

	@Override
	public Long save(NewsModel newsModel) {
		String sql = "insert into new (title,content,category_id) values (?,?,?)";
		return insert(sql, newsModel.getTitle(), newsModel.getContent(), newsModel.getCategory_id());
	}

	@Override
	public void delete(Long id) {
		String sql = "delete from new where id = ?";
		update(sql, id);
	}

	@Override
	public void Edit(NewsModel newsModel) {
		String sql = "update new set content = ?,  title= ?  where id = ?";
		update(sql, newsModel.getContent(),newsModel.getTitle(),newsModel.getId());
	}

	@Override
	public NewsModel findone(Long id) {
		String sql = "Select *from new where id = ?";
		List<NewsModel> news = query(sql, new NewMapper(), id);
		return news.isEmpty() ? null : news.get(0);
	}

	@Override
	public List<NewsModel> findAll() {
		StringBuffer sql = new StringBuffer("Select *from new");
		return query(sql.toString(), new NewMapper());
	}

	@Override
	public int Count() {
		String sql = "select count(*) from new";
		return count(sql);
	}

	@Override
	public List<NewsModel> findAll(Pageble pageble) {
		StringBuilder sql = new StringBuilder("SELECT * FROM new");
		if (pageble.getSortter() != null) {
			sql.append(" ORDER BY "+pageble.getSortter().getSortName()+" "+pageble.getSortter().getSortBy()+"");
		}
		if (pageble.getOffset() != null && pageble.getLimit() != null) {
			sql.append(" LIMIT "+pageble.getOffset()+", "+pageble.getLimit()+"");
		}
		return query(sql.toString(), new NewMapper());
	}



}
