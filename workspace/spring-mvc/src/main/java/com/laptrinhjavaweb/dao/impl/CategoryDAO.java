package com.laptrinhjavaweb.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.laptrinhjavaweb.dao.ICategoryDAO;
import com.laptrinhjavaweb.mapper.CategoryMapper;
import com.laptrinhjavaweb.model.CategoryModel;
@Repository
public class CategoryDAO extends AbstractDAO<CategoryModel> implements ICategoryDAO {


	@Override
	public List<CategoryModel> findAll() {
		String sql = "select * from category";
		return query(sql, new CategoryMapper());
	}

	@Override
	public Long save(CategoryModel model) {
		String sql = "insert into category(name,code) values(?, ?)";
		return insert(sql, model.getName(),model.getCode());
	}

	@Override
	public CategoryModel findone(Long id) {
		String sql = "select * from spring.category where id = ?";
		List<CategoryModel> model = query(sql, new CategoryMapper(), id);
		return model.isEmpty() ? null : model.get(0);
	}
	
	public static void main(String[] args) {
		CategoryDAO a = new CategoryDAO();
		System.out.println(a.findone(1L));
	}
}
