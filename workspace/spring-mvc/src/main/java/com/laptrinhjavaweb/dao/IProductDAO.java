package com.laptrinhjavaweb.dao;

import java.util.List;

import com.laptrinhjavaweb.model.ProductModel;
import com.laptrinhjavaweb.paging.Pageble;

public interface IProductDAO extends GennericDAO<ProductModel>{
	List<ProductModel> findProductsByCategoryId(Long categoryID);
	Long save(ProductModel productModel);
	void delete(long id);
	void Edit(ProductModel productModel);
	ProductModel findOne (Long id);
	List<ProductModel> findAll();
	int Count();
	List<ProductModel> findAll(Pageble pageble);
	List<ProductModel> findProductsByCategoryId(Pageble pageble,long id);
}
