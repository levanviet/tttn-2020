package com.laptrinhjavaweb.dao;

import java.util.List;

import com.laptrinhjavaweb.model.NewsModel;
import com.laptrinhjavaweb.paging.Pageble;

public interface INewDAO extends GennericDAO<NewsModel>{
	List<NewsModel> findCategoryIDbyNews(Long categoryID);
	Long save(NewsModel newsModel);
	void delete(Long id);
	void Edit(NewsModel newsModel);
	NewsModel findone (Long id);
	List<NewsModel> findAll();
	int Count();
	List<NewsModel> findAll(Pageble pageble);
}
