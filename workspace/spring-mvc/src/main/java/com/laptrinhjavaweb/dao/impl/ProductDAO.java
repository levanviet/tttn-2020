package com.laptrinhjavaweb.dao.impl;



import java.util.List;

import org.springframework.stereotype.Repository;

import com.laptrinhjavaweb.dao.IProductDAO;
import com.laptrinhjavaweb.mapper.ProductMapper;
import com.laptrinhjavaweb.model.ProductModel;
import com.laptrinhjavaweb.paging.Pageble;

@Repository
public class ProductDAO extends AbstractDAO<ProductModel> implements IProductDAO {

	@Override
	public List<ProductModel> findProductsByCategoryId(Long categoryID) {
		String sql = "select *from product where categoryid = ?";
		return query(sql, new ProductMapper(), categoryID);

	}

	@Override
	public Long save(ProductModel productModel) {
		String sql = "insert into product (name,code,thumbnail,price,categoryid) values (?,?,?,?,?)";
		return insert(sql, productModel.getName(), productModel.getCode(), productModel.getThumbnail(),
				productModel.getPrice(), productModel.getCategoryid());
	}

	@Override
	public void delete(long id) {
		String sql = "delete from product where id = ?";
		update(sql, id);
	}

	@Override
	public void Edit(ProductModel productModel) {
		String sql = "update product set name = ?, code = ? ," + "thumbnail= ?,price= ?, size = ?,categoryid= ?  "
				+ " where id = ?";
		update(sql, productModel.getName(), productModel.getCode(), productModel.getThumbnail(),
				productModel.getPrice(),productModel.getSize(), productModel.getCategoryid(), productModel.getId());
	}

	@Override
	public ProductModel findOne(Long id) {
		String sql = "select *from product where id = ?";
		List<ProductModel> products = query(sql, new ProductMapper(), id);
		return products.isEmpty() ? null : products.get(0);
	}

	@Override
	public List<ProductModel> findAll() {
		StringBuffer sql = new StringBuffer("select *from product");
		return query(sql.toString(), new ProductMapper());
	}

	@Override
	public int Count() {
		String sql = "select count(*) from product";
		return count(sql);
	}

	@Override
	public List<ProductModel> findAll(Pageble pageble) {
		StringBuilder sql = new StringBuilder("SELECT * FROM product");
		if (pageble.getSortter() != null) {
			sql.append(" ORDER BY " + pageble.getSortter().getSortName() + " " + pageble.getSortter().getSortBy() + "");
		}
		if (pageble.getOffset() != null && pageble.getLimit() != null) {
			sql.append(" LIMIT " + pageble.getOffset() + ", " + pageble.getLimit() + "");
		}
		return query(sql.toString(), new ProductMapper());
	}

	public static void main(String[] args) {
		ProductDAO d = new ProductDAO();
		//CategoryDAO c = new CategoryDAO();
		//List<ProductModel> model = new ArrayList<>();
		//ProductModel model = new ProductModel();
		System.out.println(d.findProductsByCategoryId(1l));
	}

	@Override
	public List<ProductModel> findProductsByCategoryId(Pageble pageble, long id) {
		StringBuilder sql = new StringBuilder("SELECT * FROM product where categoryid = ?");
		if (pageble.getSortter() != null) {
			sql.append(" ORDER BY " + pageble.getSortter().getSortName() + " " + pageble.getSortter().getSortBy() + "");
		}
		if (pageble.getOffset() != null && pageble.getLimit() != null) {
			sql.append(" LIMIT " + pageble.getOffset() + ", " + pageble.getLimit() + "");
		}
		return query(sql.toString(), new ProductMapper(),id);
	}

}
