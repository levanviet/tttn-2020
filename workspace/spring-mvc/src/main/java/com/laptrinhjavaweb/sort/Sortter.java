package com.laptrinhjavaweb.sort;

public class Sortter {
	private String sortName;
	private String sortBy;
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public Sortter(String sortName, String sortBy) {
		this.sortName = sortName;
		this.sortBy = sortBy;
	}
	
}
