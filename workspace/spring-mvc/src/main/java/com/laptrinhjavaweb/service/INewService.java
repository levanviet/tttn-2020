package com.laptrinhjavaweb.service;

import java.util.List;

import com.laptrinhjavaweb.model.NewsModel;
import com.laptrinhjavaweb.paging.Pageble;

public interface INewService {
	List<NewsModel> findCategoryIDbyNews(Long categoryID);
	NewsModel save(NewsModel model);
	List<NewsModel> findAll();
	NewsModel update(NewsModel model);
	void delete(long[] ids);
	int getTotalItem();
	NewsModel findOne(Long id);
	List<NewsModel> findAll(Pageble pageble);
}
