package com.laptrinhjavaweb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.laptrinhjavaweb.dao.IProductDAO;
import com.laptrinhjavaweb.model.ProductModel;
import com.laptrinhjavaweb.paging.Pageble;
import com.laptrinhjavaweb.service.IProductService;
@Service
public class ProductService implements IProductService{

	@Autowired
	private IProductDAO productDAO;
	
	@Override
	public List<ProductModel> findProductsByCategoryId(Long categoryID) {
		return productDAO.findProductsByCategoryId(categoryID);
	}

	@Override
	public ProductModel save(ProductModel model) {
		Long id = productDAO.save(model);
		return productDAO.findOne(id);
	}

	@Override
	public List<ProductModel> findAll() {
		
		return productDAO.findAll();
	}

	@Override
	public ProductModel update(ProductModel model) {
		productDAO.Edit(model);
		return productDAO.findOne(model.getId());
	}

	@Override
	public void delete(ProductModel model) {
		for(Long id : model.getIds()) {
			productDAO.delete(id);
		}
		
	}

	@Override
	public int getTotalItem() {
		
		return productDAO.Count();
	}

	@Override
	public ProductModel findOne(Long id) {
		return productDAO.findOne(id);
	}

	@Override
	public List<ProductModel> findAll(Pageble pageble) {
		
		return productDAO.findAll(pageble);
	}

	@Override
	public List<ProductModel> findProductsByCategoryId(Pageble pageble, long id) {
		
		return productDAO.findProductsByCategoryId(pageble,id);
	}

}
