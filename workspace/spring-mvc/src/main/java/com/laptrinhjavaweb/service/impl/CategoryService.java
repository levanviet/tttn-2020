package com.laptrinhjavaweb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.laptrinhjavaweb.dao.ICategoryDAO;
import com.laptrinhjavaweb.model.CategoryModel;
import com.laptrinhjavaweb.service.ICategoryService;
@Service
public class CategoryService implements ICategoryService{

//	private ICategoryDAO categoryDAO;
//	public CategoryService() {
//		categoryDAO = new CategoryDAO(); interface k dao ra doi tuong chi dung de ke thua
//	}
	
	@Autowired
	private ICategoryDAO categoryDAO;
	
	
	@Override
	public List<CategoryModel> findAll() {
		return categoryDAO.findAll();
		
	}
	@Override
	public Long save(CategoryModel model) {	
		return categoryDAO.save(model);
	}
	@Override
	public CategoryModel findOne(Long id) {
		return categoryDAO.findone(id);
	}

	
}
