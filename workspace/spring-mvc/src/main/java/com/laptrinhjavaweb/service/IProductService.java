package com.laptrinhjavaweb.service;

import java.util.List;

import com.laptrinhjavaweb.model.ProductModel;
import com.laptrinhjavaweb.paging.Pageble;

public interface IProductService {
	List<ProductModel> findProductsByCategoryId(Long categoryID);
	ProductModel save(ProductModel model);
	List<ProductModel> findAll();
	ProductModel update(ProductModel model);
	void delete(ProductModel productModel);
	int getTotalItem();
	ProductModel findOne(Long id);
	List<ProductModel> findAll(Pageble pageble);
	List<ProductModel> findProductsByCategoryId(Pageble pageble,long id);
}
