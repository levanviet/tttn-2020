package com.laptrinhjavaweb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.laptrinhjavaweb.dao.INewDAO;
import com.laptrinhjavaweb.model.NewsModel;
import com.laptrinhjavaweb.paging.Pageble;
import com.laptrinhjavaweb.service.INewService;


@Service
public class NewService implements INewService {

	@Autowired
	private INewDAO newDAO;
	//private ICommentDAO commentDAO;

	@Override
	public List<NewsModel> findCategoryIDbyNews(Long categoryID) {
		return newDAO.findCategoryIDbyNews(categoryID);
	}

	@Override
	public NewsModel save(NewsModel model) {
		Long id = newDAO.save(model);
		return newDAO.findone(id);
	}

	@Override
	public List<NewsModel> findAll() {
		return newDAO.findAll();
	}

	@Override
	public NewsModel update(NewsModel model) {
		newDAO.Edit(model);
		return newDAO.findone(model.getId());

	}

	@Override
	public void delete(long[] ids) {
		for (long id : ids) {
			//commentDAO.deleteByNewID(id);
			newDAO.delete(id);
		}
	}

	@Override
	public int getTotalItem() {
		return newDAO.Count();
	}

	@Override
	public NewsModel findOne(Long id) {
		return newDAO.findone(id);
	}

	@Override
	public List<NewsModel> findAll(Pageble pageble) {
		
		return newDAO.findAll(pageble);
	}

}
