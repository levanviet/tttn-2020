<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="dec" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- <meta name="viewport" -->
<!-- 	content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
<!-- <meta name="description" content=""> -->
<!-- <meta name="author" content=""> -->
<!-- Bootstrap core CSS -->
<link
	href=" <c:url value='/template/web/vendor/bootstrap/css/bootstrap.min.css'/>"
	rel="stylesheet">
<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
<!-- Custom styles for this template -->
<link href=" <c:url value='/template/web/css/small-business.css' /> "
	rel="stylesheet">
<title>Trang chủ</title>
</head>
<body>
	<%@include file="/common/web/header.jsp"%>
	<%@include file="/common/web/menu.jsp"%>
	<dec:body />
	<%@include file="/common/web/footer.jsp"%>
<script src="<c:url value='/template/web/vendor/jquery/jquery.min.js'/>"></script>
  <script src="<c:url value='/template/web/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>
</body>
</html>