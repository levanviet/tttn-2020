<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<c:url var='APIurl' value='/api/new'/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit</title>
</head>
<body>

	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
					</li>
				</ul>
				<!-- /.breadcrumb -->

			</div>

			<div class="page-content">
				<form id="formSubmit">
				
					
<!-- 					<div class="form-group"> -->
<!-- 						<label class="control-label col-sm-2" for="pwd">Id</label> -->
<!-- 						<div class="col-sm-10"> -->
<!-- 							<input  class="form-control" id="id" name="id" -->
<%-- 								placeholder="" value="${models.id}" hindden> --%>
<!-- 						</div> -->
<!-- 					</div> -->
					
<!-- 					<br><br><br> -->
					<input type="hidden" id="id" name="id" value="${models.id}"/>
					<div class="form-group">
					 <label class="control-label col-sm-2" for="pwd">Category</label>
						<div class="col-sm-10">
						 
						  <select class="form-control" id="categoryid" name="categoryid">
						  	<option value='${models.categoryid}' selected="selected">${models.categoryid}</option>
						  	<c:forEach var = 'item' items='${categorys.listResult}'>
						  		<option value='${item.id}' >${item.name}</option>
						  	</c:forEach>
						    
						  </select>
<!-- 						  <input  class="form-control" id="categoryid" name="categoryid" -->
<%-- 								placeholder="" value="${models.categoryid}"> --%>
<!-- 						 </div> -->
					</div>
					</div>
					<br><br><br>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Name</label>
						<div class="col-sm-10">
							<input class="form-control" id="name" name="name"
								placeholder="" value="${models.name}">
						</div>
					</div>
					
					<br><br><br>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Code</label>
						<div class="col-sm-10">
							<input class="form-control" id="code" name="code"
								placeholder="" value="${models.code}">
						</div>
					</div>
					
					<br><br><br>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Price</label>
						<div class="col-sm-10">
							<input  class="form-control" id="price"
								placeholder="" value="${models.price}" name="price">
						</div>
					</div>
					
					<br><br><br>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Size</label>
						<div class="col-sm-10">
							<input  class="form-control" id="size" name="size"
								placeholder="" value="${models.size}"/>
						</div>
					</div>
					
					<br><br><br>
					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Thumbnail</label>
						<div class="col-sm-10">
							<textarea rows="" cols="" name="thumbnail" id="thumbnail">${models.thumbnail}</textarea>
						</div>
					</div>
					<br><br><br>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						<c:if test="${empty models.id }">
							<input  type="button" class="btn btn-default" id = "btnUpdateOrInsert" value="Thêm sản phẩm"/>
						</c:if>
						<c:if test="${not empty models.id }">
							<input type="button" class="btn btn-default" id = "btnUpdateOrInsert" value="Sửa sản phẩm"/>
						</c:if>
						</div>
					</div>
				</form>
			</div>
			<!-- /.page-content -->
		</div>
	</div>
	<script type="text/javascript">
		var editor = "";
		$(document).ready(function(){
			editor = CKEDITOR.replace('thumbnail');
		});
		$('#btnUpdateOrInsert').click(function(e){
			e.preventDefault(); // tranh submit form nham. co the submit tai cai trang hien tai.
			var data = {}; // mang javascript object
			//var categoryCode = $('#categoryCode').val(); // lay ra dc cac ra tri trong option tuong ung
			//var title = $('#title').val;
			//var content = $('#content').val();
			// = > thay bằng serializeArray();
			var formData = $('#formSubmit').serializeArray(); // lay ra danh sach gom co: name - value trong formData
			console.log(data);
			$.each(formData,function(i,v){ // i: index, v: value
				data[""+v.name+""] = v.value; // gán cho mang data gom name - value tuong ung: data[title,content,...] = {thể thao,the thao,...}
			});
			data['thumbnail'] = editor.getData();
			var id = $('#id').val();
			if(id == ""){
				addNew(data);
			}
			else{
				updateNew(data);
			}
		});
		
		function addNew(data){
			$.ajax({
				url: '/spring-mvc/api/new/add',
				type : 'POST',
				contentType: 'application/json',
				data: JSON.stringify(data), // chuyen doi javascript object thanh json(key - value)
				dataType : 'json',
				success: function(result){
					window.location.href = '/spring-mvc/admin/new/list?maxPageItem=3&page=1&sortBy=asc&sortName=id&categoryid=1';
				},
				error: function(error){
					//window.location.href = '/spring-mvc/admin/new/list?maxPageItem=3&page=1&sortBy=asc&sortName=name';
					console.log(error);
				}
			});
		}
		
		function updateNew(data){
			$.ajax({
				url: '/spring-mvc/api/new/update',
				type : 'PUT',
				contentType : 'application/json',
				data : JSON.stringify(data),// chuyen doi javascript object sang json: vi trong server dinh nghia kieu su lieu truyen vao la json
				dataType: 'json', // du lieu server tra ve
				success: function(result){
					window.location.href = '/spring-mvc/admin/new/list?maxPageItem=3&page=1&sortBy=asc&sortName=id&categoryid=1';
					console.log(result);
				},
				error: function(error){
					console.log(error);
				}
			});
		}
	</script>
</body>
</html>