<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Insert title here</title>
</head>
<body>
	<div class="main-content">
				<table class="table table-bordered table-sm">
						<thead>
							<tr>
								<th>Option</th>
								<th>Name</th>
								<th>Price</th>
								<th>Thumbnail</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var='item' items='${models.listResult}'>
								<tr>
									<td><input type = "checkbox" name="checkbox_${item.id}" id="checkbox_${item.id}" value="${item.id}"></td>
									<td>${item.name}</td>
									<td>${item.price}</td>
									<td>${item.thumbnail}</td>
									<td>	
										<a href=" <c:url value='/admin/new/edit?id=${item.id}' />"  data-toggle="tooltip" title="Sửa bài viết"><i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i></a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			</div>
</body>
</html>