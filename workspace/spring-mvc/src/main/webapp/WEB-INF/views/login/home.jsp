<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
 <%@include file="/common/taglib.jsp"%>
<!DOCTYPE html> 
<html>
<head>
<meta charset="UTF-8">
<title>Đăng nhập</title>
</head>
<body>
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h2 style="color: orange">Đăng nhập</h2>
				<div class="d-flex justify-content-end social_icon">
					<span><i class="fab fa-facebook-square"></i></span> <span><i
						class="fab fa-google-plus-square"></i></span> <span><i
						class="fab fa-twitter-square"></i></span>
				</div>
			</div>
			<div class="card-body">
				<form action="j_spring_security_check" id="formlogin" method="post">
					<c:if test="${param.incorrectAccount != null}">
						<div class="alert alert-danger">
							<strong>User or password Incorrect</strong>
						</div>
					</c:if>


					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" class="form-control" placeholder="username"
							name="j_username">

					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" class="form-control" placeholder="password"
							name="j_password">
					</div>
					<div class="form-group">
						<input type="submit" value="Login"
							class="btn float-right login_btn">
					</div>
					<div class="form-group">
						<a href=" <c:url value='/trang-chu'/> "
							class="btn alert-danger float-left">Logout</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>