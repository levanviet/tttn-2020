<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="dec" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE html>
<html>
<head>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<!-- <meta name="viewport" -->
<!-- 	content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
<!-- <meta name="description" content=""> -->
<!-- <meta name="author" content=""> -->
<!-- Custom fonts for this template-->
<link href=" <c:url value='/template/admin/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet"
	type="text/css">

<!-- Page level plugin CSS-->
<link href=" <c:url value='/template/admin/vendor/datatables/dataTables.bootstrap4.css'/>" rel="stylesheet">

<!-- Custom styles for this template-->
<link href=" <c:url value='/template/admin/css/sb-admin.css'/>" rel="stylesheet">
<title>Quản trị</title>
</head>
<body id="page-top">
	<%@include file="/common/admin/header_.jsp"%>
	<div id="wrapper">
		<%@include file="/common/admin/menu_.jsp"%>
		<div id="content-wrapper">
			<dec:body />
			<%@include file="/common/admin/footer_.jsp"%>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript-->
	<script src="<c:url value='/template/admin/vendor/jquery/jquery.min.js'/>"></script>
	<script src="<c:url value='/template/admin/vendor/bootstrap/js/bootstrap.bundle.min.js'/>"></script>

	<!-- Core plugin JavaScript-->
	<script src="<c:url value='/template/admin/vendor/jquery-easing/jquery.easing.min.js'/>"></script>

	<!-- Page level plugin JavaScript-->
	<script src="<c:url value='/template/admin/vendor/chart.js/Chart.min.js'/> "></script>
	<script src="<c:url value='/template/admin/vendor/datatables/jquery.dataTables.js'/> "></script>
	<script src="<c:url value='/template/admin/vendor/datatables/dataTables.bootstrap4.js'/> "></script>

	<!-- Custom scripts for all pages-->
	<script src="<c:url value='/template/admin/js/sb-admin.min.js'/>"></script>

	<!-- Demo scripts for this page-->
	<script src="<c:url value='/template/admin/js/demo/datatables-demo.js'/>"></script>
	<script src="<c:url value='/template/admin/js/demo/chart-area-demo.js'/>"></script>
</body>
</html>