<!-- Navigation -->
<%@page import="com.laptrinhjavaweb.util.SecurityUtils" %>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="#" style="color: fff;font-size:30px">Evadeeva</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive" aria-controls="navbarResponsive"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<securiry:authorize access="isAnonymous()" >
					<li class="nav-item"><a class="nav-link" href="<c:url value='/dang-nhap'/>">Sign in</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Register</a></li>
				</securiry:authorize>
				<securiry:authorize access="isAuthenticated()" >
					<li class="nav-item"><a class="nav-link" href="#">Hello <%=SecurityUtils.getPrincipal().getFullName() %></a></li>
					<li class="nav-item"><a class="nav-link" href="<c:url value='/thoat' />">Sign out</a></li>
				</securiry:authorize>
				
			</ul>
		</div>
	</div>
</nav>