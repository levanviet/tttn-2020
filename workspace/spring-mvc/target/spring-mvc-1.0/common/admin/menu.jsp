<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<div id="sidebar"
	class="sidebar                  responsive                    ace-save-state"
	data-sidebar="true" data-sidebar-scroll="true"
	data-sidebar-hover="true">
	<script type="text/javascript">
		try {
			ace.settings.loadState('sidebar')
		} catch (e) {
		}
	</script>

	<div class="sidebar-shortcuts" id="sidebar-shortcuts">
		<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
			<button class="btn btn-success">
				<i class="ace-icon fa fa-signal"></i>
			</button>

			<button class="btn btn-info">
				<i class="ace-icon fa fa-pencil"></i>
			</button>

			<button class="btn btn-warning">
				<i class="ace-icon fa fa-users"></i>
			</button>

			<button class="btn btn-danger">
				<i class="ace-icon fa fa-cogs"></i>
			</button>
		</div>

		<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
			<span class="btn btn-success"></span> <span class="btn btn-info"></span>

			<span class="btn btn-warning"></span> <span class="btn btn-danger"></span>
		</div>
	</div>
	<!-- /.sidebar-shortcuts -->

	<ul class="nav nav-list" style="top: 0px;">

		<li class=""><a href="#" class="dropdown-toggle"> <i
				class="menu-icon fa fa-desktop"></i> <span class="menu-text">
					Category </span> <b class="arrow fa fa-angle-down"></b>
		</a> <b class="arrow"></b>

			<ul class="submenu">
				<!-- 							<li class=""> -->
				<!-- 								<a href="#" class="dropdown-toggle"> -->
				<!-- 									<i class="menu-icon fa fa-caret-right"></i> -->

				<!-- 									Layouts -->
				<!-- 									<b class="arrow fa fa-angle-down"></b> -->
				<!-- 								</a> -->

				<!-- 								<b class="arrow"></b> -->

				<!-- 								<ul class="submenu"> -->
				<!-- 									<li class=""> -->
				<!-- 										<a href="top-menu.html"> -->
				<!-- 											<i class="menu-icon fa fa-caret-right"></i> -->
				<!-- 											Top Menu -->
				<!-- 										</a> -->

				<!-- 										<b class="arrow"></b> -->
				<!-- 									</li> -->

				<!-- 									<li class=""> -->
				<!-- 										<a href="two-menu-1.html"> -->
				<!-- 											<i class="menu-icon fa fa-caret-right"></i> -->
				<!-- 											Two Menus 1 -->
				<!-- 										</a> -->

				<!-- 										<b class="arrow"></b> -->
				<!-- 									</li> -->

				<!-- 									<li class=""> -->
				<!-- 										<a href="two-menu-2.html"> -->
				<!-- 											<i class="menu-icon fa fa-caret-right"></i> -->
				<!-- 											Two Menus 2 -->
				<!-- 										</a> -->

				<!-- 										<b class="arrow"></b> -->
				<!-- 									</li> -->

				<!-- 									<li class=""> -->
				<!-- 										<a href="mobile-menu-1.html"> -->
				<!-- 											<i class="menu-icon fa fa-caret-right"></i> -->
				<!-- 											Default Mobile Menu -->
				<!-- 										</a> -->

				<!-- 										<b class="arrow"></b> -->
				<!-- 									</li> -->

				<!-- 									<li class=""> -->
				<!-- 										<a href="mobile-menu-2.html"> -->
				<!-- 											<i class="menu-icon fa fa-caret-right"></i> -->
				<!-- 											Mobile Menu 2 -->
				<!-- 										</a> -->

				<!-- 										<b class="arrow"></b> -->
				<!-- 									</li> -->

				<!-- 									<li class=""> -->
				<!-- 										<a href="mobile-menu-3.html"> -->
				<!-- 											<i class="menu-icon fa fa-caret-right"></i> -->
				<!-- 											Mobile Menu 3 -->
				<!-- 										</a> -->

				<!-- 										<b class="arrow"></b> -->
				<!-- 									</li> -->
				<!-- 								</ul> -->
				<!-- 							</li> -->

				<li class="">
					<a href=" <c:url value='/admin/new/list?page=1&maxPageItem=3&sortName=title&sortBy=desc'/>"> 
						<i class="menu-icon fa fa-caret-right"></i> Áo
					</a>
					 <b class="arrow"></b>
				</li>

				<li class=""><a href="elements.html"> <i
						class="menu-icon fa fa-caret-right"></i> Đầm
				</a> <b class="arrow"></b></li>

				<li class=""><a href="buttons.html"> <i
						class="menu-icon fa fa-caret-right"></i> Váy
				</a> <b class="arrow"></b></li>

				<li class=""><a href="content-slider.html"> <i
						class="menu-icon fa fa-caret-right"></i> Quần
				</a> <b class="arrow"></b></li>

				<li class=""><a href="treeview.html"> <i
						class="menu-icon fa fa-caret-right"></i> Áo khoác
				</a> <b class="arrow"></b></li>


				<li class=""><a href="#" class="dropdown-toggle"> <i
						class="menu-icon fa fa-caret-right"></i> Phụ kiện <b
						class="arrow fa fa-angle-down"></b>
				</a> <b class="arrow"></b>

					<ul class="submenu">
						<li class="">
							<a href="#"> <i class="menu-icon fa fa-leaf green"></i> Giầy </a> 
							<b class="arrow"> </b>
						</li>
						<li class="">
							<a href="#"> <i class="menu-icon fa fa-leaf green"></i> Túi xách </a> 
							<b class="arrow"> </b>
						</li>
						<li class="">
							<a href="#"> <i class="menu-icon fa fa-leaf green"></i> Khăn </a> 
							<b class="arrow"> </b>
						</li>
						<li class="">
							<a href="#"> <i class="menu-icon fa fa-leaf green"></i> Thắt lưng </a> 
							<b class="arrow"> </b>
						</li>
						<li class="">
							<a href="#"> <i class="menu-icon fa fa-leaf green"></i> Bông tai </a> 
							<b class="arrow"> </b>
						</li>
						<li class="">
							<a href="#"> <i class="menu-icon fa fa-leaf green"></i> Vòng cổ </a> 
							<b class="arrow"> </b>
						</li>

<!-- 						<li class=""><a href="#" class="dropdown-toggle"> <i -->
<!-- 								class="menu-icon fa fa-pencil orange"></i> Túi xách <b -->
<!-- 								class="arrow fa fa-angle-down"></b> -->
<!-- 						</a> <b class="arrow"></b> -->

<!-- 							<ul class="submenu"> -->
<!-- 								<li class=""><a href="#"> <i -->
<!-- 										class="menu-icon fa fa-plus purple"></i> Add Product -->
<!-- 								</a> <b class="arrow"></b></li> -->

<!-- 								<li class=""><a href="#"> <i -->
<!-- 										class="menu-icon fa fa-eye pink"></i> View Products -->
<!-- 								</a> <b class="arrow"></b></li> -->
<!-- 							</ul> -->
							
<!-- 							</li> -->
					</ul></li>
			</ul></li>


	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon"
			class="ace-icon fa fa-angle-double-left ace-save-state"
			data-icon1="ace-icon fa fa-angle-double-left"
			data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>