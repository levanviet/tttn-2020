<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/taglib.jsp"%>
<!DOCTYPE html>
<html>

<head>


<title>Danh sách bài viết</title>
</head>

<body>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">New</a></li>
				</ul>
				<!-- /.breadcrumb -->

			</div>

			<div class="page-content">
				<button class="btn btn-danger btn-white btn-bold" type="button" id="btnDelete">
					<i class="fa fa-trash-o fa-3x" aria-hidden="true"></i>
				</button>

				<a class="btn btn-primary" href=" <c:url value='/admin/new/edit' /> " aria-label="Plus">
  					<i class="fa fa-plus-circle fa-3x" aria-hidden="true"></i>
				</a>
				<form action="" id="form"
					method="get">
					<table class="table table-bordered table-sm">
						<thead>
							<tr>
								<th>Option</th>
								<th>Name</th>
								<th>Code</th>
								<th>Price</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var='item' items='${models.listResult}'>
								<tr>
									<td><input type = "checkbox" name="checkbox_${item.id}" id="checkbox_${item.id}" value="${item.id}"></td>
									<td>${item.name}</td>
									<td>${item.code}</td>
									<td>${item.price}</td>
									<td>	
										<a href=" <c:url value='/admin/new/edit?id=${item.id}' />"  data-toggle="tooltip" title="Sửa bài viết"><i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i></a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<ul class="pagination" id="pagination"></ul>
						<input type="hidden" id="page" name="page" />
						<input type="hidden" id="maxPageItem" name="maxPageItem" />
						<input type="hidden" id="sortName" name="sortName" />
						<input type="hidden" id="sortBy" name="sortBy" />
<!-- 						<input type="hidden" id="type" name="type" /> -->
				</form>
			</div>
			<!-- /.page-content -->
		</div>
	</div>
	<!-- /.main-content -->
	<script>
		$(document).ready(function(){
  			$('[data-toggle="tooltip"]').tooltip(); 
  			
		});
	</script>
	<script>
		var currentPage = ${models.page};
		var totalPage = ${models.totalPage};
		var limit = 3;
		$(function() {

			window.pagObj = $('#pagination').twbsPagination({
				totalPages : totalPage,
				visiblePages : 3,
				startPage : currentPage,// để cho trang đứng im tại vị trí chọn.Trang bắt đầu phải bằng số trang lúc chọn
				onPageClick : function(event, page) {
					//Hiện tại 2 trang vẫn bắng nhau nên => Khi chưa chọn trang khác nó vẫn thực hiện submit form
					// Mỗi lần chọn trang khác thì page != currentPage
					// Vì nó sẽ chạy function (event, page) trước => sẽ có gt của page trước
					// sau đó mới gán cho input -> model -> currentPage.
					if (currentPage != page) {
						$('#maxPageItem').val(limit);// sau khi load xong thì maxPageItem k có gt nên phải gán=limit
						$('#page').val(page);
						$('#sortName').val('name');
						$('#sortBy').val('desc');
						$('#form').submit();
					}
				}
			}).on('page', function(event, page) {
				console.info(page + ' (from event listening)');
			});
		});
		
		
		$('#btnDelete').click(function(){
			var data = {};
			console.log(data);
			var ids = $('tbody input[type=checkbox]:checked').map(function(){
				return $(this).val();
			}).get();
			data['ids'] = ids;  // <=> ids = [1,2,3,4,5,6]
			deleteNew(data);
			
		});
		
		function deleteNew(data){
			$.ajax({
				url: '/spring-mvc/api/new/delete',
				type : 'DELETE',
				contentType : 'application/json',
				data : JSON.stringify(data),// chuyen doi javascript object sang json: vi trong server dinh nghia kieu su lieu truyen vao la json // du lieu server tra ve
				success: function(result){
 					WINDOW.LOCATION.HREF = '/SPRING-MVC/ADMIN/NEW/LIST?MAXPAGEITEM=3&PAGE=1&SORTBY=ASC&SORTNAME=TITLE';
// 					//WINDOW.LOCATION.HREF = '/JSP-SERVLET-DEMO1/ADMIN-NEW-LIST?MAXPAGEITEM=3&TYPE=LIST&PAGE=1&SORTBY=ASC&SORTNAME=TITLE';
					console.log(result);
				},
				error: function(error){
					console.log(result);
				}
			});
		}
	</script>
</body>

</html>