<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Trang chủ</title>
</head>
<body>
	<!-- Page Content -->
	<div class="container">

		<div class="row">

			<div class="col-lg-3">

				<h1 class="my-4">Thể loại</h1>
				<div class="list-group">
				
					<c:forEach var="item" items="${categorys.listResult}">
						<a href="<c:url value='/dam?categoryid=${item.id }'/>" class="list-group-item">${item.name}</a> 
					</c:forEach>

				</div>

			</div>
			<!-- /.col-lg-3 -->

			<div class="col-lg-9">

				<div id="carouselExampleIndicators" class="carousel slide my-4"
					data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0"
							class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item active">
							<img class="d-block img-fluid" src="<c:url value='/template/web/img/slideshow_1.webp'/>"
								alt="First slide">
						</div>
						<div class="carousel-item">
							<img class="d-block img-fluid" src="<c:url value='/template/web/img/slideshow_2.jpg'/>"
								alt="Second slide">
						</div>
						<div class="carousel-item">
							<img class="d-block img-fluid" src="<c:url value='/template/web/img/slideshow_1.webp'/>"
								alt="Third slide">
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators"
						role="button" data-slide="prev"> <span
						class="carousel-control-prev-icon" aria-hidden="true"></span> <span
						class="sr-only">Previous</span>
					</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
						role="button" data-slide="next"> <span
						class="carousel-control-next-icon" aria-hidden="true"></span> <span
						class="sr-only">Next</span>
					</a>
				</div>

				<div class="row">
					<c:if test="${empty dams.listResult }">
					<c:forEach var="item" items="${products.listResult}">
						<div class="col-lg-4 col-md-6 mb-4">
							<div class="card h-100">
								<a href="#"><img class="card-img-top"
									src="" alt=""></a>
								<div class="card-body">
									
									<h5>${item.price}</h5>
									<p class="card-text">${item.name}</p>
									<h4 class="card-title">
										<a href="#">Mua</a>
									</h4>
								</div>
								<div class="card-footer">
									<small class="text-muted">&#9733; &#9733; &#9733;
										&#9733; &#9734;</small>
								</div>
							</div>
						</div>
					</c:forEach>
					</c:if>
					<c:if test="${not empty dams.listResult}">
						<c:forEach var="item" items="${products.listResult }">
						<div class="col-lg-4 col-md-6 mb-4">
							<div class="card h-100">
								<a href="#"><img class="card-img-top"
									src="https://drive.google.com/file/d/1x1L3avr6ksU7CCVR1d6L4sowr8DRkpNa/preview" alt=""></a>
								<div class="card-body">
									
									<h5>${item.price}</h5>
									<p class="card-text">${item.name}</p>
									<h4 class="card-title">
										<a href="#">Mua</a>
									</h4>
								</div>
								<div class="card-footer">
									<small class="text-muted">&#9733; &#9733; &#9733;
										&#9733; &#9734;</small>
								</div>
							</div>
						</div>
					</c:forEach>
					</c:if>

				</div>
				<!-- /.row -->

			</div>
			<!-- /.col-lg-9 -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->



</body>
</html>